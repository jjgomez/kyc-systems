<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Asignaciones</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Asignaciones</h3>
              <div class="p-2">
                <button type="button" class="btn btn-success float-right" onclick="add();">Agregar</button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="asignacionesTables" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="width:1%">Id</th>
                  <th style="width:10%">Matricula</th>
                  <th style="width:10%">Nombre Alumno</th>
                  <th style="width:10%">Nombre Materia</th>
                  <th style="width:10%">Clave Materia</th>
                  <th style="width:1%">Opciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<style>
  .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 40px;
    user-select: none;
    -webkit-user-select: none;
}
.select2-search__field{
  width: 100%;
}
</style>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar Asignaciones</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="card-body">
                <?php echo form_open('admin/addAsignacion', array('id' => 'asignacionForm')) ?>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label><span class="text-danger">*</span>Materia:</label>
                            <select id="id_materia" name="id_materia" class="select2" required="required">
                                <option value="">Select Materia</option>
                                <?php
                              foreach($all_materias as $materia)
                              {

                                echo '<option value="'.$materia['id'].'">'.$materia['clave_materia'].' - '.$materia['nombre'].'</option>';
                              }
                              ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label><span class="text-danger">*</span>Alumnos:</label>
                            <select id="id_alumno" name="id_alumno[]" class="select2" required="required" multiple="multiple">
                                <?php
                                  foreach($all_alumnos as $alumnos)
                                  {
                                    echo '<option value="'.$alumnos['id'].'">'.$alumnos['nombre'].'</option>';
                                  }
                                  ?>
                            </select>
                        </div>
                    </div>
                  
                <?php echo form_close() ?>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary controller-asignacion">Save</button>
      </div>
    </div>
  </div>
</div>

<script>

  function add() {

    document.getElementById("asignacionForm").reset();
    $("#id_materia").select2({
					placeholder: "Select",
          width: '100%'
		});
    $("#id_alumno").select2({
					placeholder: "Select",
          width: '100%'
		});
    $('#exampleModal').modal('show');
  }

  function remove(id) { 

    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this company!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

          $.ajax({
              url: '<?php echo base_url(); ?>admin/remove',
              type: 'POST',
              dataType: "json",
              data: {
                  id: id,
                  entity: 'asignaciones',
              },
              success: function(response) {

                if (response.status=='success') {

                    var table = $('#asignacionesTables').DataTable();
                    table.ajax.reload();
                    swal("Deleted!", "has been deleted.", "success");

                } else {
                    $.notify(response.message, "error");
                }
              },
              error: function(jqXHR, exception) {
                $.notify(jqXHR.responseText, "error");
              }
          });

        } else {
        swal("Cancelled", "Your Alumno is safe :", "error");
      }
    });
  };
</script>