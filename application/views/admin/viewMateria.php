<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Materias</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Materias</h3>
              <div class="p-2">
                <button type="button" class="btn btn-success float-right" onclick="add();">Agregar</button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="materiasTables" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="width:1%">Id</th>
                  <th style="width:10%">Clave Materia</th>
                  <th style="width:10%">Nombre</th>
                  <th style="width:1%">Opciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar Nuevas Materias</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card-body">
                <?php echo form_open('admin/addMateria', array('id' => 'materiaForm')) ?>
                  <div class="row">
                    <div class="col-sm-12">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Clave Materia:</label>
                        <input type="hidden" class="form-control" name="id" id="id">
                        <input type="text" class="form-control" placeholder="Enter ..." name="clave_materia" id="clave_materia" maxlength="4" onkeypress="return validaNumericos(event)">
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" class="form-control" placeholder="Enter ..." name="nombre" id="nombre" min="1" maxlength="100">
                      </div>
                    </div>
                  </div>
                <?php echo form_close() ?>
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary controller-materia">Save</button>
      </div>
    </div>
  </div>
</div>

<script>

  function add() {

    document.getElementById("materiaForm").reset();
    $('#id').val('');
    $('#exampleModal').modal('show');
  }

  function edit(id) {

    $.ajax({
        url: "<?php echo base_url(); ?>admin/getAllData",
        type: 'POST',
        dataType: "json",
        data: {
            id: id,
            entity: 'materias',
        },
        success: function(response) {
            if (response) {

              $('#clave_materia').val(response.clave_materia);
              $("#nombre").val(response.nombre);
              $("#id").val(response.id);
              $('#exampleModal').modal('show');
            }
        },
        error: function(jqXHR, exception) {
          $.notify(jqXHR.responseText, "error");
        }
    });
  }

  function remove(id) { 

    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this company!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

          $.ajax({
              url: '<?php echo base_url(); ?>admin/remove',
              type: 'POST',
              dataType: "json",
              data: {
                  id: id,
                  entity: 'materias',
              },
              success: function(response) {

                if (response.status=='success') {

                    var table = $('#materiasTables').DataTable();
                    table.ajax.reload();
                    swal("Deleted!", "has been deleted.", "success");

                } else {
                    $.notify(response.message, "error");
                }
              },
              error: function(jqXHR, exception) {
                $.notify(jqXHR.responseText, "error");
              }
          });

        } else {
        swal("Cancelled", "Your Alumno is safe :", "error");
      }
    });
  };

    function validaNumericos(event) {
        if(event.charCode >= 48 && event.charCode <= 57){
          return true;
         }
         return false;        
    }
</script>